/*
 Navicat Premium Data Transfer

 Source Server         : local
 Source Server Type    : MySQL
 Source Server Version : 50721
 Source Host           : localhost:3306
 Source Schema         : hotel

 Target Server Type    : MySQL
 Target Server Version : 50721
 File Encoding         : 65001

 Date: 06/08/2018 00:59:43
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for DailyRate
-- ----------------------------
DROP TABLE IF EXISTS `DailyRate`;
CREATE TABLE `DailyRate` (
  `ddate` date NOT NULL,
  `rmType` varchar(3) NOT NULL,
  `rateType` varchar(3) NOT NULL,
  `refRule` int(9) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `rate` double(255,2) NOT NULL,
  PRIMARY KEY (`ddate`,`rmType`,`rateType`),
  KEY `refRul` (`refRule`),
  CONSTRAINT `refRul` FOREIGN KEY (`refRule`) REFERENCES `RateRule` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of DailyRate
-- ----------------------------
BEGIN;
INSERT INTO `DailyRate` VALUES ('2018-08-01', 'DLT', 'B1B', 000000007, 899.99);
INSERT INTO `DailyRate` VALUES ('2018-08-02', 'DLT', 'B1B', 000000007, 899.99);
INSERT INTO `DailyRate` VALUES ('2018-08-02', 'SPK', 'B2B', 000000008, 223.34);
INSERT INTO `DailyRate` VALUES ('2018-08-05', 'SPT', 'P2B', 000000009, 863.45);
INSERT INTO `DailyRate` VALUES ('2018-08-06', 'SPT', 'P2B', 000000009, 863.45);
INSERT INTO `DailyRate` VALUES ('2018-08-07', 'SPT', 'P2B', 000000009, 863.45);
INSERT INTO `DailyRate` VALUES ('2018-08-08', 'DLT', 'B1B', 000000007, 899.99);
INSERT INTO `DailyRate` VALUES ('2018-08-09', 'DLT', 'B1B', 000000007, 899.99);
INSERT INTO `DailyRate` VALUES ('2018-08-09', 'ECK', 'B1B', 000000006, 100.08);
INSERT INTO `DailyRate` VALUES ('2018-08-09', 'SPK', 'B2B', 000000008, 223.34);
INSERT INTO `DailyRate` VALUES ('2018-08-11', 'ECK', 'B1B', 000000006, 100.08);
INSERT INTO `DailyRate` VALUES ('2018-08-12', 'ECK', 'B1B', 000000006, 100.08);
INSERT INTO `DailyRate` VALUES ('2018-08-12', 'SPT', 'P2B', 000000009, 863.45);
INSERT INTO `DailyRate` VALUES ('2018-08-13', 'SPT', 'P2B', 000000009, 863.45);
INSERT INTO `DailyRate` VALUES ('2018-08-14', 'ECK', 'B1B', 000000006, 100.08);
INSERT INTO `DailyRate` VALUES ('2018-08-14', 'SPT', 'P2B', 000000009, 863.45);
INSERT INTO `DailyRate` VALUES ('2018-08-15', 'DLT', 'B1B', 000000007, 899.99);
INSERT INTO `DailyRate` VALUES ('2018-08-15', 'ECK', 'B1B', 000000006, 100.08);
INSERT INTO `DailyRate` VALUES ('2018-08-16', 'DLT', 'B1B', 000000007, 899.99);
INSERT INTO `DailyRate` VALUES ('2018-08-16', 'ECK', 'B1B', 000000006, 100.08);
INSERT INTO `DailyRate` VALUES ('2018-08-16', 'SPK', 'B2B', 000000008, 223.34);
INSERT INTO `DailyRate` VALUES ('2018-08-18', 'ECK', 'B1B', 000000006, 100.08);
INSERT INTO `DailyRate` VALUES ('2018-08-19', 'ECK', 'B1B', 000000006, 100.08);
INSERT INTO `DailyRate` VALUES ('2018-08-19', 'SPT', 'P2B', 000000009, 863.45);
INSERT INTO `DailyRate` VALUES ('2018-08-20', 'SPT', 'P2B', 000000009, 863.45);
INSERT INTO `DailyRate` VALUES ('2018-08-21', 'ECK', 'B1B', 000000006, 100.08);
INSERT INTO `DailyRate` VALUES ('2018-08-21', 'SPT', 'P2B', 000000009, 863.45);
INSERT INTO `DailyRate` VALUES ('2018-08-22', 'DLT', 'B1B', 000000007, 899.99);
INSERT INTO `DailyRate` VALUES ('2018-08-22', 'ECK', 'B1B', 000000006, 100.08);
INSERT INTO `DailyRate` VALUES ('2018-08-23', 'DLT', 'B1B', 000000007, 899.99);
INSERT INTO `DailyRate` VALUES ('2018-08-23', 'ECK', 'B1B', 000000006, 100.08);
INSERT INTO `DailyRate` VALUES ('2018-08-23', 'SPK', 'B2B', 000000008, 223.34);
INSERT INTO `DailyRate` VALUES ('2018-08-25', 'ECK', 'B1B', 000000006, 100.08);
INSERT INTO `DailyRate` VALUES ('2018-08-26', 'ECK', 'B1B', 000000006, 100.08);
INSERT INTO `DailyRate` VALUES ('2018-08-26', 'SPT', 'P2B', 000000009, 863.45);
INSERT INTO `DailyRate` VALUES ('2018-08-27', 'SPT', 'P2B', 000000009, 863.45);
INSERT INTO `DailyRate` VALUES ('2018-08-28', 'ECK', 'B1B', 000000006, 100.08);
INSERT INTO `DailyRate` VALUES ('2018-08-28', 'SPT', 'P2B', 000000009, 863.45);
INSERT INTO `DailyRate` VALUES ('2018-08-29', 'DLT', 'B1B', 000000007, 899.99);
INSERT INTO `DailyRate` VALUES ('2018-08-29', 'ECK', 'B1B', 000000006, 100.08);
INSERT INTO `DailyRate` VALUES ('2018-08-30', 'DLT', 'B1B', 000000007, 899.99);
INSERT INTO `DailyRate` VALUES ('2018-08-30', 'ECK', 'B1B', 000000006, 100.08);
INSERT INTO `DailyRate` VALUES ('2018-08-30', 'SPK', 'B2B', 000000008, 223.34);
COMMIT;

-- ----------------------------
-- Table structure for DailyStatus
-- ----------------------------
DROP TABLE IF EXISTS `DailyStatus`;
CREATE TABLE `DailyStatus` (
  `ddate` date NOT NULL,
  `rmType` varchar(3) NOT NULL,
  `sold` int(255) NOT NULL DEFAULT '0',
  `other` int(11) NOT NULL DEFAULT '0',
  `refRule` int(8) DEFAULT NULL,
  PRIMARY KEY (`ddate`,`rmType`),
  KEY `roomType` (`rmType`),
  CONSTRAINT `roomType` FOREIGN KEY (`rmType`) REFERENCES `RoomType` (`code`) ON DELETE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of DailyStatus
-- ----------------------------
BEGIN;
INSERT INTO `DailyStatus` VALUES ('2018-07-01', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-01', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-02', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-02', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-02', 'SPK', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-03', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-03', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-03', 'SPK', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-04', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-04', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-04', 'SPK', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-05', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-05', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-05', 'SPK', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-06', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-06', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-06', 'SPK', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-07', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-07', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-07', 'SPK', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-08', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-08', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-08', 'SPK', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-09', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-09', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-09', 'SPK', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-10', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-10', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-10', 'SPK', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-11', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-11', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-11', 'SPK', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-12', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-12', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-12', 'SPK', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-13', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-13', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-13', 'SPK', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-14', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-14', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-14', 'SPK', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-15', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-15', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-15', 'SPK', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-16', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-16', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-16', 'SPK', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-17', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-17', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-17', 'SPK', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-18', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-18', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-18', 'SPK', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-19', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-19', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-19', 'SPK', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-20', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-20', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-20', 'SPK', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-21', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-21', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-21', 'SPK', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-22', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-22', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-22', 'SPK', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-23', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-23', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-23', 'SPK', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-24', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-24', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-24', 'SPK', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-25', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-25', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-26', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-26', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-27', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-27', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-28', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-28', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-29', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-29', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-30', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-30', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-31', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-07-31', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-01', 'DLK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-01', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-01', 'DLT', 0, 3, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-01', 'ECK', 0, -5, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-01', 'SPK', 0, 3, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-01', 'SPT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-02', 'DLK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-02', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-02', 'DLT', 0, 3, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-02', 'ECK', 0, -5, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-02', 'SPK', 0, 3, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-02', 'SPT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-03', 'DLK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-03', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-03', 'DLT', 0, 3, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-03', 'ECK', 0, -5, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-03', 'SPK', 0, 3, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-03', 'SPT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-04', 'DLK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-04', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-04', 'DLT', 0, 3, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-04', 'ECK', 0, -5, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-04', 'SPK', 0, 3, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-04', 'SPT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-05', 'DLK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-05', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-05', 'DLT', 0, 3, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-05', 'ECK', 0, -5, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-05', 'SPK', 0, 3, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-05', 'SPT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-06', 'DLK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-06', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-06', 'DLT', 0, 3, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-06', 'ECK', 0, -5, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-06', 'SPK', 0, 3, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-06', 'SPT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-07', 'DLK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-07', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-07', 'DLT', 0, 3, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-07', 'ECK', 0, -5, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-07', 'SPK', 0, 3, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-07', 'SPT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-08', 'DLK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-08', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-08', 'DLT', 0, 3, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-08', 'ECK', 0, -5, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-08', 'SPK', 0, 3, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-08', 'SPT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-09', 'DLK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-09', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-09', 'DLT', 0, 3, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-09', 'ECK', 0, -5, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-09', 'SPK', 0, 3, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-09', 'SPT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-10', 'DLK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-10', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-10', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-10', 'ECK', 0, -5, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-10', 'SPK', 0, 3, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-10', 'SPT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-11', 'DLK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-11', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-11', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-11', 'ECK', 0, -5, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-11', 'SPK', 0, 3, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-11', 'SPT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-12', 'DLK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-12', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-12', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-12', 'ECK', 0, -5, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-12', 'SPK', 0, 3, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-12', 'SPT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-13', 'DLK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-13', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-13', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-13', 'ECK', 0, -5, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-13', 'SPK', 0, 3, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-13', 'SPT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-14', 'DLK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-14', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-14', 'DLT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-14', 'ECK', 0, -5, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-14', 'SPK', 0, 3, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-14', 'SPT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-15', 'DLK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-15', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-15', 'DLT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-15', 'ECK', 0, -5, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-15', 'SPK', 0, 3, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-15', 'SPT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-16', 'DLK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-16', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-16', 'DLT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-16', 'ECK', 0, -5, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-16', 'SPK', 0, 3, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-16', 'SPT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-17', 'DLK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-17', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-17', 'DLT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-17', 'ECK', 0, -5, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-17', 'SPK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-17', 'SPT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-18', 'DLK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-18', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-18', 'DLT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-18', 'ECK', 0, -5, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-18', 'SPK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-18', 'SPT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-19', 'DLK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-19', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-19', 'DLT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-19', 'ECK', 0, -5, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-19', 'SPK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-19', 'SPT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-20', 'DLK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-20', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-20', 'DLT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-20', 'ECK', 0, -5, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-20', 'SPK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-20', 'SPT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-21', 'DLK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-21', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-21', 'DLT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-21', 'ECK', 0, -5, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-21', 'SPK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-21', 'SPT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-22', 'DLK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-22', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-22', 'DLT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-22', 'ECK', 0, -5, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-22', 'SPK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-22', 'SPT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-23', 'DLK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-23', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-23', 'DLT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-23', 'ECK', 0, -5, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-23', 'SPK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-23', 'SPT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-24', 'DLK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-24', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-24', 'DLT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-24', 'ECK', 0, -5, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-24', 'SPK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-24', 'SPT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-25', 'DLK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-25', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-25', 'DLT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-25', 'ECK', 0, -5, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-25', 'SPK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-25', 'SPT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-26', 'DLK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-26', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-26', 'DLT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-26', 'ECK', 0, -5, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-26', 'SPK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-26', 'SPT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-27', 'DLK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-27', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-27', 'DLT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-27', 'ECK', 0, -5, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-27', 'SPK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-27', 'SPT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-28', 'DLK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-28', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-28', 'DLT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-28', 'ECK', 0, -5, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-28', 'SPK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-28', 'SPT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-29', 'DLK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-29', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-29', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-29', 'ECK', 0, -5, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-29', 'SPK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-29', 'SPT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-30', 'DLK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-30', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-30', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-30', 'ECK', 0, -5, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-30', 'SPK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-30', 'SPT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-31', 'DLK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-31', 'DLS', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-31', 'DLT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-31', 'SPK', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-08-31', 'SPT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-09-01', 'SPT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-09-02', 'SPT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-09-03', 'SPT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-09-04', 'SPT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-09-05', 'SPT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-09-06', 'SPT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-09-07', 'SPT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-09-08', 'SPT', 0, 2, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-09-09', 'SPT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-09-10', 'SPT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-09-11', 'SPT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-09-12', 'SPT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-09-13', 'SPT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-09-14', 'SPT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-09-15', 'SPT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-09-16', 'SPT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-09-17', 'SPT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-09-18', 'SPT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-09-19', 'SPT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-09-20', 'SPT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-09-21', 'SPT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-09-22', 'SPT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-09-23', 'SPT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-09-24', 'SPT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-09-25', 'SPT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-09-26', 'SPT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-09-27', 'SPT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-09-28', 'SPT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-09-29', 'SPT', 0, 0, NULL);
INSERT INTO `DailyStatus` VALUES ('2018-09-30', 'SPT', 0, 0, NULL);
COMMIT;

-- ----------------------------
-- Table structure for RateRule
-- ----------------------------
DROP TABLE IF EXISTS `RateRule`;
CREATE TABLE `RateRule` (
  `id` int(9) unsigned zerofill NOT NULL AUTO_INCREMENT COMMENT 'Rate Rule ID',
  `fromDate` date NOT NULL COMMENT 'Rate Effect Date',
  `toDate` date NOT NULL COMMENT 'Rate Force Date',
  `rateType` varchar(3) NOT NULL COMMENT 'Rate Type code',
  `rmType` varchar(3) NOT NULL COMMENT 'Room Type Code',
  `mon` tinyint(1) DEFAULT NULL COMMENT 'Monday',
  `tue` tinyint(1) DEFAULT NULL COMMENT 'Tuesday',
  `wed` tinyint(1) DEFAULT NULL COMMENT 'Wednesday',
  `thu` tinyint(1) DEFAULT NULL COMMENT 'Thursday',
  `fri` tinyint(1) DEFAULT NULL COMMENT 'Friday',
  `sat` tinyint(1) DEFAULT NULL COMMENT 'Saturday',
  `sun` tinyint(1) DEFAULT '0' COMMENT 'Sunday',
  `rate` double(255,2) NOT NULL COMMENT 'Rate',
  `notes` text COMMENT 'Notes',
  PRIMARY KEY (`id`),
  UNIQUE KEY `id` (`id`),
  KEY `rateType` (`rateType`),
  KEY `rmType1` (`rmType`),
  CONSTRAINT `rateType` FOREIGN KEY (`rateType`) REFERENCES `RateType` (`code`) ON DELETE CASCADE,
  CONSTRAINT `rmType1` FOREIGN KEY (`rmType`) REFERENCES `RoomType` (`code`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of RateRule
-- ----------------------------
BEGIN;
INSERT INTO `RateRule` VALUES (000000006, '2018-08-09', '2018-08-30', 'B1B', 'ECK', 0, 1, 1, 1, 0, 1, 1, 100.08, 'hello, for test\r\njjj\r\ndajhfad');
INSERT INTO `RateRule` VALUES (000000007, '2018-08-01', '2018-08-31', 'B1B', 'DLT', 0, 0, 1, 1, 0, 0, 0, 899.99, '');
INSERT INTO `RateRule` VALUES (000000008, '2018-08-01', '2018-08-31', 'B2B', 'SPK', NULL, NULL, NULL, 1, NULL, NULL, 0, 223.34, '');
INSERT INTO `RateRule` VALUES (000000009, '2018-08-01', '2018-08-31', 'P2B', 'SPT', 1, 1, NULL, NULL, NULL, NULL, 1, 863.45, '');
COMMIT;

-- ----------------------------
-- Table structure for RateType
-- ----------------------------
DROP TABLE IF EXISTS `RateType`;
CREATE TABLE `RateType` (
  `code` varchar(3) NOT NULL COMMENT 'Rate Type abbr',
  `name` varchar(255) NOT NULL COMMENT 'Rate Type name',
  `prepaid` tinyint(1) NOT NULL COMMENT 'Whether prepaid',
  `freeCancelDate` int(11) NOT NULL COMMENT 'Free Cancellation Deadline',
  `isToAll` tinyint(1) NOT NULL COMMENT 'Whether a public rate',
  `description` text COMMENT 'rule to use the rate type',
  PRIMARY KEY (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of RateType
-- ----------------------------
BEGIN;
INSERT INTO `RateType` VALUES ('B1B', 'Preferred rate + 1 breakfast', 0, 1, 1, 'Includes daily breakfast for 1 guest. ');
INSERT INTO `RateType` VALUES ('B2B', 'Preferred rate + breakfast', 0, 1, 1, 'Includes daily breakfast for 2 guest. ');
INSERT INTO `RateType` VALUES ('BIA', 'Book in advance and save', 1, 999, 1, 'This reservation must be booked and paid in advance. It is completely non-refundable.');
INSERT INTO `RateType` VALUES ('GPP', 'Global Preferred Program', 0, 1, 0, 'Company ID, Business card or company Invitation letter Required upon Check-in.');
INSERT INTO `RateType` VALUES ('P1B', 'Prepaid with 1 Breakfast', 1, 999, 1, 'Includes daily 1 breakfast. Fully guaranteed and non changeable.');
INSERT INTO `RateType` VALUES ('P2B', 'Prepaid with 2 Breakfast', 1, 999, 1, 'Includes daily 2 breakfast. Fully guaranteed and non changeable.');
INSERT INTO `RateType` VALUES ('PFR', 'Preferred Rate', 0, 1, 1, 'GTD/DEPOSIT POLICY\r\n\r\nPlease note that any change in your reservation may change the rate and/or require payment of cancellation fees.\r\nFor reservations guaranteed with a form of payment at time of booking, rooms are held until hotel check-out time the day following arrival. For reservations not guaranteed with a form of payment at time of booking, rooms are held until set cancellation time per the rules of the reservation. In the event more guests arrive than can be accommodated due to hotel overbooking or an unforeseen circumstance, and hotel is unable to hold rooms consistent with this room hold policy, hotel will attempt to accommodate guests, at its expense, at a comparable hotel in the area for the oversold night(s), and will pay for transportation to that hotel.\r\nAGE REQUIREMENT POLICY\r\nGuests must be 18 years or older to check-in.\r\nFAMILY PLAN POLICY\r\nRoom rates include the accommodation of children (17 years old or younger) who sleep in the existing bedding of a guest room. Rollaway beds and cribs may incur extra charges.\r\nEXTRA CHARGES\r\nNote: At this hotel guests 17 and under are considered children. All other guests are considered adults.\r\nExtra rollaway: CNY 313.64 per rollaway/per night.\r\nChild rollaway: CNY 300.00 per rollaway/per night.\r\nCrib: No charge\r\nNote: not all rooms may be able to accommodate rollaways or cribs. Please contact the hotel directly for more information.');
INSERT INTO `RateType` VALUES ('PWC', 'PWC Corporate Rate', 0, 1, 0, 'RATE INCLUDES BKFST AND HSIA.WORKING ID OR BUSINESS CARD REQUIRED UPON CHECK-IN.Identification Required.');
COMMIT;

-- ----------------------------
-- Table structure for Reservation
-- ----------------------------
DROP TABLE IF EXISTS `Reservation`;
CREATE TABLE `Reservation` (
  `confirmID` int(10) unsigned zerofill NOT NULL AUTO_INCREMENT COMMENT 'Confirmation Number',
  `ciDate` date NOT NULL COMMENT 'Check-in Date',
  `coDate` date NOT NULL COMMENT 'Check-out Date',
  `adults` int(11) NOT NULL COMMENT 'Adult number',
  `child` int(11) NOT NULL COMMENT 'Child number',
  `quantity` int(255) NOT NULL COMMENT 'quantity of room',
  `rmType` varchar(3) NOT NULL COMMENT 'Room Type Code',
  `rateType` varchar(3) NOT NULL COMMENT 'Rate Type Code',
  `amount` double(255,2) NOT NULL COMMENT 'Total fee',
  `title1` varchar(3) NOT NULL COMMENT 'Guest1 title',
  `name1` varchar(255) NOT NULL COMMENT 'Guest1 Fisrt name',
  `surname1` varchar(255) NOT NULL COMMENT 'Guest1 Surname',
  `title12` varchar(3) DEFAULT NULL COMMENT 'Guest2 title',
  `name2` varchar(255) DEFAULT NULL COMMENT 'Guest2 Fisrt name',
  `surname2` varchar(255) DEFAULT NULL COMMENT 'Guest2 Surname',
  `address` text COMMENT 'Guest address',
  `city` varchar(255) DEFAULT NULL COMMENT 'Guest city',
  `zip` varchar(0) DEFAULT NULL COMMENT 'Guest Zip Code',
  `country` varchar(255) DEFAULT NULL COMMENT 'Guest Country',
  `phone` varchar(255) NOT NULL COMMENT 'Guest phone number',
  `cardType` varchar(255) NOT NULL COMMENT 'Credit card type',
  `cardNumber` varchar(0) NOT NULL COMMENT 'Credit card number',
  `cardExpire` varchar(4) DEFAULT NULL COMMENT 'Expire date,format:mmyy',
  `status` varchar(255) NOT NULL COMMENT 'Reservation status',
  `freeCancelDate` datetime NOT NULL COMMENT 'Free Cancellation deadline',
  `from` varchar(255) NOT NULL COMMENT 'Reservation from',
  `madeDate` datetime NOT NULL COMMENT 'Made datetime',
  `madeBy` int(5) NOT NULL COMMENT 'Made staff id',
  `notes` text,
  PRIMARY KEY (`confirmID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Table structure for RoomRule
-- ----------------------------
DROP TABLE IF EXISTS `RoomRule`;
CREATE TABLE `RoomRule` (
  `id` int(9) unsigned zerofill NOT NULL AUTO_INCREMENT,
  `fromDate` date NOT NULL COMMENT 'Start Date',
  `toDate` date NOT NULL COMMENT 'End Date',
  `rmType` varchar(3) NOT NULL COMMENT 'Room Type abbr',
  `num` int(11) NOT NULL COMMENT 'quantity of unuse',
  `notes` text,
  PRIMARY KEY (`id`),
  KEY `rmType` (`rmType`),
  CONSTRAINT `rmType` FOREIGN KEY (`rmType`) REFERENCES `RoomType` (`code`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of RoomRule
-- ----------------------------
BEGIN;
INSERT INTO `RoomRule` VALUES (000000002, '2018-08-01', '2018-08-16', 'SPK', 3, 'hello for test');
INSERT INTO `RoomRule` VALUES (000000004, '2018-08-01', '2018-08-09', 'DLT', 3, 'å?ºç?£æ??');
INSERT INTO `RoomRule` VALUES (000000005, '2018-08-14', '2018-08-28', 'DLT', 2, 'No test');
INSERT INTO `RoomRule` VALUES (000000006, '2018-07-02', '2018-07-24', 'SPK', 2, 'å¤§å¤§å??');
COMMIT;

-- ----------------------------
-- Table structure for RoomType
-- ----------------------------
DROP TABLE IF EXISTS `RoomType`;
CREATE TABLE `RoomType` (
  `code` varchar(3) NOT NULL COMMENT 'Room Type Abbr',
  `name` varchar(255) NOT NULL COMMENT 'Room Type Name',
  `num` int(11) NOT NULL COMMENT 'Total quantity of the room type',
  `bed` int(11) NOT NULL COMMENT 'the quantity of bed',
  `tier` int(255) NOT NULL COMMENT 'the level of the room type',
  `description` text COMMENT 'Room Type Description',
  PRIMARY KEY (`code`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of RoomType
-- ----------------------------
BEGIN;
INSERT INTO `RoomType` VALUES ('DLK', 'Deluxe Room', 90, 1, 2, '- 46 Square Meters\n-Dual Vanities (Select Rooms)\n-Walk-In Wardrobe\n-Disability Accessible Rooms\n-Heavenly Rollaway Bed Available\n-Makeup Vanity (Select Rooms)\n-River, City, or Financial District Views\n-Bathroom with River Views\n-Connecting Rooms Available\n-Rainforest Shower');
INSERT INTO `RoomType` VALUES ('DLS', 'Duplex Suite', 3, 1, 7, '-256 Square Meters\n-Freestanding Soaking Bathtub\n-Dining Table for Eight\n-Dual Vanities\n-Private Pool\n-Heavenly Rollaway Bed Available\n-Westin Executive Club Lounge Access\n-One Bedroom\n-Two Bathrooms\n-Three LCD TVs\n-Open Kitchen\n-Bathroom with Floor-to-Ceiling Windows\n-White Tea by Westin™ Bath Amenities\n-Study Room (Select Suites)\n-Rainforest Shower\n-Panoramic River, Mountain, or Dam Views\n-Two Floors\n-Walk-In Wardrobe\n');
INSERT INTO `RoomType` VALUES ('DLT', 'Deluxe Room', 90, 2, 2, '- 46 Square Meters\r\n-Dual Vanities (Select Rooms)\r\n-Walk-In Wardrobe\r\n-Disability Accessible Rooms\r\n-Heavenly Rollaway Bed Available\r\n-Makeup Vanity (Select Rooms)\r\n-River, City, or Financial District Views\r\n-Bathroom with River Views\r\n-Connecting Rooms Available\r\n-Rainforest Shower');
INSERT INTO `RoomType` VALUES ('ECK', 'Westin Executive Club Room', 21, 1, 3, '-36~46 Square Meters\r\n-White Tea by Westin™ Bath Amenities\r\n-Panoramic River, Mountain, or City Views\r\n-Rainforest Shower\r\n-In-Room Check-In (Upon Request)\r\n-Pressing of Two Garments (on arrival day only)\r\n-Two Hours Use of Club Lounge Meeting Room\r\n-Walk-In Wardrobe\r\n-Heavenly Rollaway Bed Available\r\n-4pm Late Checkout (Subject to Availability)\r\n-Westin Executive Club Lounge Access\r\n-Makeup Vanity (Select Rooms)');
INSERT INTO `RoomType` VALUES ('ECS', 'Westin Executive Club Suite', 12, 1, 6, '-83 Square Meters\n-Rainforest Shower\n-Two Hours Use of Club Lounge Meeting Room\n-In-Room Check-In (Upon Request)\n-Panoramic River or Mountain Views\n-Walk-In Wardrobe\n-Freestanding Soaking Bathtub\n-Pressing of Two Garments (on arrival day only)\n-Dual Vanities\n-Heavenly Rollaway Bed Available\n-4pm Late Checkout (Subject to Availability)\n-Westin Executive Club Lounge Access\n-Two Bathrooms\n-Three LCD TVs\n-Bathroom with Floor-to-Ceiling Windows\n-White Tea by Westin™ Bath Amenities');
INSERT INTO `RoomType` VALUES ('ECT', 'Westin Executive Club Room', 21, 2, 3, '-36~46 Square Meters\n-White Tea by Westin™ Bath Amenities\n-Panoramic River, Mountain, or City Views\n-Rainforest Shower\n-In-Room Check-In (Upon Request)\n-Pressing of Two Garments (on arrival day only)\n-Two Hours Use of Club Lounge Meeting Room\n-Walk-In Wardrobe\n-Heavenly Rollaway Bed Available\n-4pm Late Checkout (Subject to Availability)\n-Westin Executive Club Lounge Access\n-Makeup Vanity (Select Rooms)');
INSERT INTO `RoomType` VALUES ('NIJ', 'test', 7, 7, 8, 'iahdb\r\n\r\ndjsdä½ å¥½');
INSERT INTO `RoomType` VALUES ('PDS', 'Presidential Suite', 1, 1, 8, '-336 Square Meters\n-Two Bedrooms\n-Bathroom with Floor-to-Ceiling Windows\n-White Tea by Westin™ Bath Amenities\n-Study\n-Three Bathrooms\n-Rainforest Shower\n-Treadmill\n-Walk-In Wardrobe\n-River and Financial District Views\n-Living Room with Work Area\n-Freestanding Soaking Bathtub\n-Pantry\n-Dining/Meeting Area for Ten\n-Dual Vanities\n-Heavenly Rollaway Bed Available\n-Four LCD TVs\n-Full Kitchen\n-Sauna and Steam Room\n-52nd Floor Location\n-Westin Executive Club Lounge Access');
INSERT INTO `RoomType` VALUES ('RNS', 'Renewal Suite', 12, 1, 5, '-63 Square Meters\n-Dual Vanities\n-Walk-In Wardrobe\n-Heavenly Rollaway Bed Available\n-River or City Views\n-Westin Executive Club Lounge Access\n-Bathroom with Floor-to-Ceiling Windows\n-White Tea by Westin™ Bath Amenities\n-Two LCD TVs\n-Rainforest Shower\n\n');
INSERT INTO `RoomType` VALUES ('SPK', 'Superior Room', 15, 1, 1, '-36 Square Meters\r\n-River, City, or Financial District Views');
INSERT INTO `RoomType` VALUES ('SPT', 'Superior Room', 15, 2, 1, '-36 Square Meters\r\n-River, City, or Financial District Views');
INSERT INTO `RoomType` VALUES ('STS', 'Studio Suite', 35, 1, 4, '-58~73 Square Meters\n-Bathroom with Floor-to-Ceiling Windows\n-River, Mountain, or City Views\n-White Tea by Westin™ Bath Amenities\n-Two LCD TVs\n-Fitness Amenities in Select Studios\n-Dual Vanities\n-Walk-In Wardrobe\n-Heavenly Rollaway Bed Available');
COMMIT;

-- ----------------------------
-- Table structure for Staff
-- ----------------------------
DROP TABLE IF EXISTS `Staff`;
CREATE TABLE `Staff` (
  `id` int(5) unsigned zerofill NOT NULL AUTO_INCREMENT COMMENT 'Staff ID',
  `pd` varchar(16) NOT NULL COMMENT 'password',
  `name` varchar(255) NOT NULL COMMENT 'Staff First Name',
  `surname` varchar(255) NOT NULL COMMENT 'Staff Last Name',
  `dept` varchar(255) NOT NULL COMMENT 'Department name',
  `position` varchar(255) NOT NULL COMMENT 'Position',
  `notes` text,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68949 DEFAULT CHARSET=utf8mb4;

-- ----------------------------
-- Records of Staff
-- ----------------------------
BEGIN;
INSERT INTO `Staff` VALUES (68942, '123456', 'OSCAR', 'LI', 'RESERVATION', 'SPECIALIST', NULL);
INSERT INTO `Staff` VALUES (68943, '123456', 'GEOGRE', 'CHEN', 'RESERVATION', 'SPECIALIST', NULL);
INSERT INTO `Staff` VALUES (68944, '123456', 'AMELIA', 'WANG', 'REVENUE', 'SPECIALIST', NULL);
INSERT INTO `Staff` VALUES (68945, '123456', 'MIA', 'LIN', 'RESERVATION', 'MANAGER', NULL);
INSERT INTO `Staff` VALUES (68948, '123456', 'ZIHAO', 'test', 'RESERVATION', 'MANAGER', 'dddcd');
COMMIT;

-- ----------------------------
-- Triggers structure for table RateRule
-- ----------------------------
DROP TRIGGER IF EXISTS `add_rate_rule`;
delimiter ;;
CREATE TRIGGER `add_rate_rule` AFTER INSERT ON `RateRule` FOR EACH ROW BEGIN
DECLARE ddate date;
DECLARE dn varchar(255);
DECLARE p tinyint(1);
DECLARE j TINYINT(1);
SET ddate=new.fromDate;
WHILE ddate<=new.toDate DO
SELECT dayname(ddate) INTO dn;
SET p=
	CASE
    	WHEN dn='Monday' AND new.mon=1 THEN 1
        WHEN dn='Tuesday' AND new.tue=1 THEN 1
        WHEN dn='Wednesday' AND new.wed=1 THEN 1
        WHEN dn='Thursday' AND new.thu=1 THEN 1
        WHEN dn='Friday' AND new.fri=1 THEN 1
        WHEN dn='Saturday' AND new.sat=1 THEN 1
        WHEN dn='Sunday' AND new.sun=1 THEN 1
        ELSE 0
     END;
IF p=1 THEN
INSERT INTO DailyRate VALUES (ddate,new.rmtype,new.rateType,new.id, new.rate);
END IF;
SELECT COUNT(*) FROM DailyStatus d WHERE d.ddate=ddate AND d.rmType=new.rmType INTO j;
IF j=0 THEN
	INSERT INTO DailyStatus (ddate, rmType) VALUES (ddate, new.rmType);
END IF;
SELECT date_add(ddate, INTERVAL 1 day) INTO ddate;
END WHILE;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table RateRule
-- ----------------------------
DROP TRIGGER IF EXISTS `upd1_rate_rule`;
delimiter ;;
CREATE TRIGGER `upd1_rate_rule` BEFORE UPDATE ON `RateRule` FOR EACH ROW BEGIN
	DELETE FROM DailyRate WHERE refRule=old.id;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table RateRule
-- ----------------------------
DROP TRIGGER IF EXISTS `upd2_rate_rule`;
delimiter ;;
CREATE TRIGGER `upd2_rate_rule` AFTER UPDATE ON `RateRule` FOR EACH ROW BEGIN
DECLARE ddate date;
DECLARE dn varchar(255);
DECLARE p tinyint(1);
SET ddate=new.fromDate;
WHILE ddate<=new.toDate DO
SELECT dayname(ddate) INTO dn;
SET p=
	CASE
    	WHEN dn='Monday' AND new.mon=1 THEN 1
        WHEN dn='Tuesday' AND new.tue=1 THEN 1
        WHEN dn='Wednesday' AND new.wed=1 THEN 1
        WHEN dn='Thursday' AND new.thu=1 THEN 1
        WHEN dn='Friday' AND new.fri=1 THEN 1
        WHEN dn='Saturday' AND new.sat=1 THEN 1
        WHEN dn='Sunday' AND new.sun=1 THEN 1
        ELSE 0
     END;
IF p=1 THEN
INSERT INTO DailyRate VALUES (ddate,new.rmtype,new.rateType,new.id, new.rate);
END IF;
SELECT date_add(ddate, INTERVAL 1 day) INTO ddate;
END WHILE;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table Reservation
-- ----------------------------
DROP TRIGGER IF EXISTS `update_ds_on_add`;
delimiter ;;
CREATE TRIGGER `update_ds_on_add` AFTER INSERT ON `Reservation` FOR EACH ROW BEGIN
DECLARE ddate date;
SET ddate=new.ciDate;
WHILE ddate<new.coDate DO
	UPDATE DailyStatus d SET d.sold=d.sold+new.quantity WHERE d.rmType=new.rmType AND d.ddate=ddate;
	SELECT DATE_ADD(ddate,INTERVAL 1 DAY) INTO ddate;
END WHILE;

END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table Reservation
-- ----------------------------
DROP TRIGGER IF EXISTS `update_ds_on_update1`;
delimiter ;;
CREATE TRIGGER `update_ds_on_update1` BEFORE UPDATE ON `Reservation` FOR EACH ROW BEGIN
DECLARE ddate date;
SET ddate=old.ciDate;
WHILE ddate<old.coDate DO
	UPDATE DailyStatus d SET d.sold=d.sold-old.quantity WHERE d.rmType=old.rmType AND d.ddate=ddate;
	SELECT DATE_ADD(ddate,INTERVAL 1 DAY) INTO ddate;
END WHILE;

END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table Reservation
-- ----------------------------
DROP TRIGGER IF EXISTS `update_ds_on_update2`;
delimiter ;;
CREATE TRIGGER `update_ds_on_update2` AFTER UPDATE ON `Reservation` FOR EACH ROW BEGIN
DECLARE ddate date;
SET ddate=new.ciDate;
WHILE ddate<new.coDate DO
	UPDATE DailyStatus d SET d.sold=d.sold+new.quantity WHERE d.rmType=new.rmType AND d.ddate=ddate;
	SELECT DATE_ADD(ddate,INTERVAL 1 DAY) INTO ddate;
END WHILE;

END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table Reservation
-- ----------------------------
DROP TRIGGER IF EXISTS `update_ds_on_delete`;
delimiter ;;
CREATE TRIGGER `update_ds_on_delete` BEFORE DELETE ON `Reservation` FOR EACH ROW BEGIN
DECLARE ddate date;
SET ddate=old.ciDate;
WHILE ddate<old.coDate DO
	UPDATE DailyStatus d SET d.sold=d.sold-old.quantity WHERE d.rmType=old.rmType AND d.ddate=ddate;
	SELECT DATE_ADD(ddate,INTERVAL 1 DAY) INTO ddate;
END WHILE;

END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table RoomRule
-- ----------------------------
DROP TRIGGER IF EXISTS `wh_add`;
delimiter ;;
CREATE TRIGGER `wh_add` AFTER INSERT ON `RoomRule` FOR EACH ROW BEGIN
DECLARE ddate date;
DECLARE dn varchar(255);
DECLARE j TINYINT(1);
SET ddate=new.fromDate;
WHILE ddate<=new.toDate DO
SELECT COUNT(*) INTO j FROM DailyStatus d WHERE d.ddate=ddate AND d.rmType=new.rmType ;
IF j=0 THEN
	INSERT INTO DailyStatus (ddate, rmType) VALUES (ddate, new.rmType);
END IF;
UPDATE DailyStatus d SET d.other=d.other+new.num WHERE d.rmType=new.rmType AND d.ddate=ddate;
SELECT date_add(ddate, INTERVAL 1 day) INTO ddate;
END WHILE;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table RoomRule
-- ----------------------------
DROP TRIGGER IF EXISTS `wh_bef_upd`;
delimiter ;;
CREATE TRIGGER `wh_bef_upd` BEFORE UPDATE ON `RoomRule` FOR EACH ROW BEGIN
DECLARE ddate date;
DECLARE dn varchar(255);
DECLARE j TINYINT(1);
SET ddate=old.fromDate;
WHILE ddate<=old.toDate DO
UPDATE DailyStatus d SET d.other=d.other-old.num WHERE d.rmType=old.rmType AND d.ddate=ddate;
SELECT date_add(ddate, INTERVAL 1 day) INTO ddate;
END WHILE;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table RoomRule
-- ----------------------------
DROP TRIGGER IF EXISTS `wh_aft_upd`;
delimiter ;;
CREATE TRIGGER `wh_aft_upd` AFTER UPDATE ON `RoomRule` FOR EACH ROW BEGIN
DECLARE ddate date;
DECLARE dn varchar(255);
DECLARE j TINYINT(1);
SET ddate=new.fromDate;
WHILE ddate<=new.toDate DO
SELECT COUNT(*) INTO j FROM DailyStatus d WHERE d.ddate=ddate AND d.rmType=new.rmType ;
IF j=0 THEN
	INSERT INTO DailyStatus (ddate, rmType) VALUES (ddate, new.rmType);
END IF;
UPDATE DailyStatus d SET d.other=d.other+new.num WHERE d.rmType=new.rmType AND d.ddate=ddate;
SELECT date_add(ddate, INTERVAL 1 day) INTO ddate;
END WHILE;
END
;;
delimiter ;

-- ----------------------------
-- Triggers structure for table RoomRule
-- ----------------------------
DROP TRIGGER IF EXISTS `wh_delete`;
delimiter ;;
CREATE TRIGGER `wh_delete` BEFORE DELETE ON `RoomRule` FOR EACH ROW BEGIN
DECLARE ddate date;
DECLARE dn varchar(255);
DECLARE j TINYINT(1);
SET ddate=old.fromDate;
WHILE ddate<=old.toDate DO
UPDATE DailyStatus d SET d.other=d.other-old.num WHERE d.rmType=old.rmType AND d.ddate=ddate;
SELECT date_add(ddate, INTERVAL 1 day) INTO ddate;
END WHILE;
END
;;
delimiter ;

SET FOREIGN_KEY_CHECKS = 1;
